.
├── LICENSE
├── README.md
├── SECURITY.md
├── SECURITY.rst
├── check_results
├── content_long_list.txt
├── content_summary.txt
├── dataset
│   └── placeholder.txt
├── dep
│   └── placeholder.txt
├── documentation
│   ├── robots.txt
│   └── scorecard
│       └── placeholder.txt
├── example
│   └── placeholder.txt
├── humans.txt
├── pyproject.toml
├── repo_check_results
│   ├── bomber.html
│   └── bomber.json
├── res
│   └── placeholder.txt
├── robots.txt
├── sbom.json
├── security.txt
├── src
│   └── placeholder.txt
├── structure.txt
├── test
│   └── placeholder.txt
└── tools
    └── placeholder.txt

12 directories, 23 files
